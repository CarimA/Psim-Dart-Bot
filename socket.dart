part of DartBot;

WebSocket connection;

Map<String, bool> JoinedRoom;
String CurrentRoom;

void Connect(String WebSocketUrl) {
  JoinedRoom = new Map<String, bool>();
  for (String room in Rooms)
    JoinedRoom[room] = false;
  CurrentRoom = "";

  WebSocket.connect(WebSocketUrl).then((socket) {
    connection = socket;
    connection.listen(onData);
  });
}

Queue<String> SendQueue = new Queue();

void Send(String room, String data) {
  // check if we're connected or if there's something to send first.
  if (data == "" || connection.readyState != WebSocket.OPEN || connection == null)
    return;

  // if the target is a lobby, it must be empty.
  if (room == "lobby")
    room = "";

  // ok, now queue it.
  String message = room + "|" + data;

  message = message.replaceAll("{{bot}}", LoginUsername);

  SendQueue.add(message);
}

void SendNext() {
  // send the next item in the queue. if there's something there.
  if (SendQueue.length > 0) {
    String next = SendQueue.removeFirst();
    print("Sending $next");
    connection.add(next);
  }

  // schedule this again.
  var future = new Future.delayed(
    new Duration(milliseconds: MessageThrottleTimer),
    SendNext
  );
}

void onData(String data)
{
  // do stuff with data here.
  print("Received $data");

  if (data.contains("\n"))
  {
    // ok, we have to split the data by lines.
    for (String s in data.split("\n"))
    {
      Parse(s);
    }
  }
  else
  {
    Parse(data);
  }
}

void Parse(String data) {
  // break up the data separated by pipes.
  List<String> Data = data.split("|");

  // if we're just a room, set it then get out.
  if (Data.length == 1) {
    List<String> RoomData = Data[0].split(">");
    if (RoomData.length == 1)
      return;
    CurrentRoom = RoomData[1];
    return;
  }

  // if the current room has nothing set, assume lobby.
  if (CurrentRoom == "")
    CurrentRoom = "lobby";

  switch (Data[1].toLowerCase()) {
    case "challstr":
    // Logging in.
      LoginToShowdown(Data[3], Data[2]);

      break;
    case "updateuser":
    // if it's not us, we're on a Guest account.
      if (Data[2] != LoginUsername)
        return;

      print("Successfully logged in!");

      for (String room in Rooms)
        Send("", "/join $room");

      break;

    case "users":
    // gotta get every user!
      for (String u in Data[2].split(",").getRange(1, Data[2].split(",").length - 1)) {
        UserEnterRoom(u.substring(1), CurrentRoom);
      }
      break;
    case "deinit":
      //SaveUsers();
      break;
    case "j":
    // check if we just joined the room if
    // said check needs to be made
      if (!JoinedRoom[CurrentRoom]) {
        if (Data[2].substring(1) == LoginUsername) {
          JoinedRoom[CurrentRoom] = true;
        }
      }

      UserEnterRoom(Data[2].substring(1), CurrentRoom);
      break;
    case "l":
      UserLeaveRoom(Data[2].substring(1), CurrentRoom);
      break;

    case "n":

      UserRename(Data[3], Data[2].substring(1));
      break;

    case "title":
    // Alert us when joining a room.
      print("Joined '" + Data[2] + "'.");

      break;

    case "pm":
      String sender = Data[2].substring(1);
      String message = Data[4];
      // for PMs, assume all users are unranked.
      String rank = " ";

      if (message.substring(0, CommandCharacter.length) == CommandCharacter) {

        String command = message.split(" ")[0].substring(CommandCharacter.length);
        List<String> args;

        if (message.indexOf(" ") > 0) {
          // this message contains parameters.

          // ~command arg1, arg2, arg3
          args = message.split(' ').getRange(1, message.split(' ').length).toList().join(' ').split(',').toList();
        }
        else {
          // this message does not contain parameters.
          args = null;
        }

        print(message);

        // personal override.
        if (Overrides.contains(SanitiseName(sender))) rank = "~";

        ActCommand(command, rank, sender, CurrentRoom, args, true);
      }

      break;
    case "c:":
      if (!JoinedRoom[CurrentRoom])
        return;

      String name = Data[3].substring(1);
      String rank = Data[3].substring(0, 1);

      if (name == LoginUsername)
        return;

      String message = Data.length > 5 ? Data.getRange(4, Data.length - 4).join("|") : Data[4];

      // check if it's a stretch.
      RegExp exp = new RegExp(r"(.)\1{7,}");
      if (exp.hasMatch(message.toLowerCase())) {
        // it's a match. warn.
        Send(CurrentRoom, "/warn $name, Automatic response: please do not stretch.");
      }

      // check if it's caps.
      if (message.length > 12 && CountCaps(message) >= message.length * 0.8) {
        // it's a match. warn.
        Send(CurrentRoom, "/warn $name, Automatic response: please do not excessively use caps.");
      }

      // ignore members who do ||.
      //if (message.length == 0)
      //  return;

      // check if it's a command
      if (message.substring(0, CommandCharacter.length) == CommandCharacter) {

        String command = message.split(" ")[0].substring(CommandCharacter.length);
        List<String> args;

        if (message.indexOf(" ") > 0) {
          // this message contains parameters.
          // ~command arg1, arg2, arg3
          args = message.split(' ').getRange(1, message.split(' ').length).toList().join(' ').split(',').toList();
        }
        else {
          // this message does not contain parameters.
          args = null;
        }

        print("Command: $message");

        // personal override.
        if (Overrides.contains(SanitiseName(name))) rank = "~";

        ActCommand(command, rank, name, CurrentRoom, args, false);
      }
      break;
  }

  ParseCommand(Data);
}

void LoginToShowdown(String challenge, String ckid) {
  print("Requesting challenge string...");

  Map<String, String> Arguments = new Map<String, String>();
  Arguments["act"] = "login";
  Arguments["name"] = LoginUsername;
  Arguments["pass"] = LoginPassword;
  Arguments["challengekeyid"] = ckid;
  Arguments["challenge"] = challenge;

  http.post("https://play.pokemonshowdown.com/action.php", body: Arguments)
  .then((http.Response e) {
    print("Received the challenge string. Attempting to log in...");
    Map response = JSON.decode(e.body.substring(1));
    Send("", "/trn $LoginUsername,0," + response["assertion"]);
  });

}