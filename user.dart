part of DartBot;

var UserCon;

void UserConnect() {
  UserCon = new ConnectionPool(host: '${SqlHost}', port: SqlPort, user: '${SqlUser}', password: '${SqlPassword}', db: '${SqlDatabase}');
}

Future UserEnterRoom(String Name, String Room) async {
  bool found = false;

  // check if this returns a Results so that we can just do the damn thing synchronously, and figure out the
  // async stuff later.
  await UserCon.query('SELECT * FROM users WHERE Names LIKE "%${SanitiseName(Name)}%";').then((results) {
    results.forEach((row) {
      if (row.Names.toString().split(',').contains(SanitiseName(Name))) {
        // they exist, add to rooms.
        int id = row.ID;
        UserCon.query('UPDATE users SET Rooms="$Room", LastRoom="", LastOnline="${new DateTime.now()}" WHERE ID=$id;');
        if (!found)
          found = true;
      }
    });
  });
}

void UserLeaveRoom(String Name, String Room) {
  UserCon.query('SELECT * FROM users WHERE Names LIKE "%${SanitiseName(Name)}%";').then((results) {
    results.forEach((row) {
      if (row.Names.toString().split(',').contains(SanitiseName(Name))) {
        int id = row.ID;
        Set<String> Rooms = row.Rooms.toString().split(',').toSet();
        Rooms.remove(Room);

        if (Rooms.length > 0)
        {
          // they're still online.
          UserCon.query('UPDATE users SET Rooms="${Rooms.join(',')}" WHERE ID=$id;');
        }
        else
        {
          // they're not online anymore.
          UserCon.query('UPDATE users SET Rooms="", LastRoom="$Room", LastOnline="${new DateTime.now()}" WHERE ID=$id;');
        }

        return;
      }
    });
  });
}

void UserRename(String OldName, String NewName)
{
  UserCon.query('SELECT * FROM users WHERE Names LIKE "%' + SanitiseName(OldName) + '%";').then((results) {
    results.forEach((row) {
      print (row.Names);
      if (row.Names.toString().split(',').toList().contains(SanitiseName(OldName))) {
        // find the right user! this is the one we want.
        int id = row.ID;
        Set<String> Names = row.Names.toString().split(',').toSet();
        Names.add(SanitiseName(NewName));

        UserCon.query('UPDATE users SET Names="${Names.join(',')}" WHERE ID=$id;');
        return;
      }
    });
  });
}