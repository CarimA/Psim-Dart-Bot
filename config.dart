part of DartBot;

// the server to connect to.
var Server = "ws://sim.smogon.com/showdown/websocket";

// the username/password to log into.
var LoginUsername = "Username";
var LoginPassword = "Password";

// the PS rooms to join.
var Rooms = ["Room" ];

// the char used as the command character.
var CommandCharacter = ".";

// the time in milliseconds to wait for the next message to be sent.
var MessageThrottleTimer = 750;

// users who should be able to use ~ commands.
var Overrides = [ "Creator" ];

// the API Key to upload to Pastee with.
var PasteeApiKey = "";

// the SQL server details.
var SqlHost = "";
var SqlPort = 0;
var SqlUser = "";
var SqlPassword = "";
var SqlDatabase = "";