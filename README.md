# Psim-Dart-Bot
A Pokémon Showdown Bot made in Dart.

Preface: This is the first time I've used Dart for non-testing purposes. A few things may be questionable or just plain wrong, and you're welcome to fix it! I ask that you please tell me how I can fix something if it is blatantly wrong!

Once you have this open, open it up in your IDE of choice, open up "config.dart", and edit all of the values to reflect what you need. The configuration values are generally self-explanitory. 

Following the Tech & Code room's general motion to support, I'll try my best to help (My username on Pokemon Showdown is Cheir, and I'm usually in the Little Cup or Tech & Code room), but if you don't understand what you're doing whether it's setting up or modifying the bot, it's quite likely that you probably shouldn't be running a Pokemon Showdown bot.
