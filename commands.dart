// TODO: make each command into its own file.

part of DartBot;

List<Command> CommandList = [];
Map<String, int> Ranks = { " " : 0, "+" : 1, "%" : 2, "@" : 3, "#" : 4, "~" : 5 };

void SetCommands() {
  print("Loading commands...");
  CommandList.add(new ShutdownCommand());
  CommandList.add(new RoomKickCommand());
  CommandList.add(new SuggestionCommand());
  CommandList.add(new CopycatCommand());
  CommandList.add(new AboutCommand());
  CommandList.add(new TimeMuteCommand());

  // this one MUST be last.
  CommandList.add(new HelpCommand());

  print("Initialising commands...");
  for (var command in CommandList)
    command.Init();
}

void ParseCommand(List<String> Data)
{
  for (var command in CommandList)
  {
    command.Parse(Data);
  }
}

void ActCommand(String Command, String Rank, String Username, String Room, List<String> Arguments, bool PM)
{
   //ok, we need to look in each command. for each one, check if the condition is satisfied.
  try {
    for (var command in CommandList) {
      if (command.Keys.contains(Command.toLowerCase()))
        if (Ranks[Rank] >= Ranks[command.RequiredRank]) {
          String response = command.Action(Username, Room, Arguments);
          response = response == null ? "" : response;
          response = response.replaceAll("{{com}}", command.Keys.join('/'));
          if (response != null) {
            if (Rank == " " || PM) {
              // respond in PMs if applicable.
              for (String r in response.split('\n'))
                Send("", "/w $Username, $r");
            }
            else {
              // post in room.
              for (String r in response.split('\n'))
                Send(Room, r);
            }
          }
          return;
        }
    }
  }
  catch (exception, stacktrace) {
    if (PM) {
      // respond in PMs if applicable.
      Send("", "/w $Username, Sorry, I couldn't process this command. Please use ~fix to make a bug report or tell Cheir. Include the command used.");
    }
    else {
      // post in room.
      Send(Room, "Sorry, I couldn't process this command. Please use ~fix to make a bug report or tell Cheir. Include the command used.");
    }

    String input = exception.toString() + "\r\n" + stacktrace.toString() + "\r\n\r\n";
    new File("data/errors.txt").writeAsStringSync(input, mode: FileMode.APPEND);
  }
}

abstract class Command {
  String RequiredRank = "%";
  List<String> Keys = [];
  String Description;

  void Init();
  void Parse(List<String> Data);
  String Action(String Username, String Room, List<String> Arguments);
}

class TimeMuteCommand extends Command {
  String RequiredRank = "%";
  List<String> Keys = [ "timemute", "tm" ];
  String Description = "Mutes a user for a specified amount of time.";

  void Init() {  }

  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments) {
    // arg[0] is name, arg[1] is time, arg[2] is optional: reason.

    // uhhh we can work out the time later.
    int time = int.parse(Arguments[1]);

    Send(Room, "/hm ${Arguments[0]}${Arguments[2] != null ? ", ${Arguments[2]}" : ""}");

    // a'ight, if it's greater than an hour, we need to make it constantly go over until it's done.

    Timer timer = new Timer(new Duration(milliseconds: (time * 1000)), () => Unmute(Room, Arguments[0]) );
  }

  void Unmute(String Room, String User)
  {
    Send(Room, "/unmute $User");
  }
}

class HelpCommand extends Command {
  String RequiredRank = " ";
  List<String> Keys = [ "help", "?", "commands" ];
  String Description = "Provides a Paste.ee with details on every usable command.";

  String commandURL;

  void Init() {
    // ok, get every command...and sort by required rank.
    List<Command> commandCopy = CommandList;
    commandCopy.sort((Command x, Command y) => Ranks[x.RequiredRank].compareTo(Ranks[y.RequiredRank]));

    String output = "";
    for (Command c in commandCopy)
    {
      // ehhhhh, for now, let's just keep this in for verbosity.
      /*if (c == this)
        continue;*/

      output += "~" + c.Keys.join(', ') + "\r\n";
      if (c.RequiredRank != " ")
        output += "Requires " + c.RequiredRank + "\r\n";
      output += c.Description + "\r\n";
      output += "\r\n";
    }

    UploadPasteee(output).then((String r) {
      commandURL = r;
    });
  }

  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments) {
    return "Commands: " + commandURL;
  }
}

class ShutdownCommand extends Command {
  String RequiredRank = "#";
  List<String> Keys = [ "shutdown", "turnoff" ];
  String Description = "Shuts down the bot.";
  void Init() { }
  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments)
  {
    exit(0);
    return null;
  }
}

class AboutCommand extends Command {
  String RequiredRank = " ";
  List<String> Keys = [ "about" ];
  String Description = "Replies with summarised information about the bot.";
  void Init() { }
  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments) {
    return "{{bot}} is a PS Bot made in Dart by Cheir.";
  }
}

class CopycatCommand extends Command {
  String RequiredRank = "@";
  List<String> Keys = [ "say", "copyme", "copycat", "do" ];
  String Description = "Makes the bot copy arg[0].";
  void Init() { }
  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments)
  {
    return Arguments.join(', ');
  }
}

class SplatoonCommand extends Command {
  String RequiredRank = " ";
  List<String> Keys = [ "splatoon" ];
  String Description = "Replies with the current Turf War/Ranked Maps and the next rotation time.";

  String output;

  void Init() {
    http.get("https://splatoon.ink/schedule.json")
    .then((http.Response e) {
      Map response = JSON.decode(e.body);
      print(response);
      DateTime end = new DateTime.fromMillisecondsSinceEpoch(response["schedule"][0]["endTime"]);
      String TurfWarMaps = "**" + response["schedule"][0]["regular"]["maps"][0]["nameEN"] + "** & **" + response["schedule"][0]["regular"]["maps"][1]["nameEN"] + "**";
      String RankedMaps = "**" + response["schedule"][0]["ranked"]["maps"][0]["nameEN"] + "** & **" + response["schedule"][0]["ranked"]["maps"][1]["nameEN"] + "**";
      String RankedRule = "**" + response["schedule"][0]["ranked"]["rulesEN"] + "**";

      output = "Current Turf War Maps are $TurfWarMaps. Current $RankedRule Maps are $RankedMaps.\nMaps will rotate in ${TimeUntil(end)}.";
    });

    var future = new Future.delayed(
        new Duration(milliseconds: 5 * 60 * 1000),
        this.Init
    );
  }

  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments)
  {
    return output;
  }
}

class SuggestionCommand extends Command {
  String RequiredRank = " ";
  List<String> Keys = [ "suggestion", "suggest", "idea", "fix", "complaint", "tip" ];
  String Description = "Allows you to make a suggestion/complaint/bug report for the bot.";
  void Init() { }
  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments) {
    String input = "\r\nSuggestion from $Username: " + Arguments[0];
    new File("data/suggestions.txt").writeAsStringSync(input, mode: FileMode.APPEND);
    return "Thank you for your suggestion.";
  }
}

class RoomKickCommand extends Command {
  String RequiredRank = "#";
  List<String> Keys = [ "roomkick", "rk" ];
  String Description = "Bans then unbans the user provided in arg[0].";
  void Init() { }
  void Parse(List<String> Data) {}
  String Action(String Username, String Room, List<String> Arguments)
  {
    if (Arguments.length == 0)
      return null;

    return "/roomban " + Arguments[0] + "\n/roomunban " + Arguments[0];
  }
}