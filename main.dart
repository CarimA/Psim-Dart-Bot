/// The DartBot library.
library DartBot;

// imports here.
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:exportable/exportable.dart';
import 'dart:convert';
import 'dart:async';
import 'dart:collection';
import 'dart:math';
import 'package:sqljocky/sqljocky.dart';
import 'package:levenshtein/levenshtein.dart';

// other parts.
part "socket.dart";
part "config.dart";
part "commands.dart";
part "user.dart";
part "functions.dart";

main(List<String> args) {
  // load the commands and connect to the sql database.
  SetCommands();
  UserConnect();

  // connect to psim.
  Connect(Server);

  // start the message sending queue.
  SendNext();
}
