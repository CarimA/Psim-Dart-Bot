part of DartBot;

String GrammarIsAre(int Count) {
  if (Count == 1)
    return "is";
  else
    return "are";
}

String GrammarPlural(String input, int count) {
  if (count == 1)
    return input;
  else
    return input + "'s";
}

// this is a generally bad method. Gotta re-write at some point.
int CountCaps(String input) {
  int count = 0;
  for (int i = 0; i < input.length; i++) {
    // ok, need to go look up how to ignore numbers + symbols.
    // for now, do things the "lazy as shit" way.
    if ([ 1, 2, 3, 4, 5, 6, 7, 8, 9].contains(input[i]))
      continue;

    // I've left the symbols that are obtained through holding Shift.
    if ([ "[", "]", ";", "'", "#", ",", ".", "/", "\\", "`"].contains(input[i]))
      continue;

    if (input[i] == input[i].toUpperCase())
      count++;
  }
  return count;
}

String TimeAgo(DateTime input)
{
  Duration difference = new DateTime.now().difference(input);
  String grammar = "";
  if (difference.inDays > 365)
  {
    int years = (difference.inDays ~/ 365);
    if (difference.inDays % 365 != 0)
      years += 1;
    grammar = years == 1 ? "year" : "years";
    return "about $years $grammar ago";
  }
  if (difference.inDays > 30)
  {
    int months = (difference.inDays ~/ 30);
    if (difference.inDays % 365 != 0)
      months += 1;
    grammar = months == 1 ? "month" : "months";
    return "about $months $grammar ago";
  }
  if (difference.inDays > 0) {
    int days = (difference.inDays);
    grammar = days == 1 ? "day" : "days";
    return "about $days $grammar ago";
  }
  if (difference.inHours > 0) {
    int hours = (difference.inHours);
    grammar = hours == 1 ? "hour" : "hours";
    return "about $hours $grammar ago";
  }
  if (difference.inMinutes > 0) {
    int minutes = (difference.inMinutes);
    grammar = minutes == 1 ? "minute" : "minutes";
    return "about $minutes $grammar ago";
  }
  int seconds = (difference.inSeconds);
  grammar = seconds == 1 ? "second" : "seconds";
  return "about $seconds $grammar ago";
}

String TimeUntil(DateTime input)
{
  Duration difference = input.difference(new DateTime.now());
  String grammar = "";
  if (difference.inDays > 365)
  {
    int years = (difference.inDays ~/ 365);
    if (difference.inDays % 365 != 0)
      years += 1;
    grammar = years == 1 ? "year" : "years";
    return "about $years $grammar time";
  }
  if (difference.inDays > 30)
  {
    int months = (difference.inDays ~/ 30);
    if (difference.inDays % 365 != 0)
      months += 1;
    grammar = months == 1 ? "month" : "months";
    return "about $months $grammar time";
  }
  if (difference.inDays > 0) {
    int days = (difference.inDays);
    grammar = days == 1 ? "day" : "days";
    return "about $days $grammar time";
  }
  if (difference.inHours > 0) {
    int hours = (difference.inHours);
    grammar = hours == 1 ? "hour" : "hours";
    return "about $hours $grammar time";
  }
  if (difference.inMinutes > 0) {
    int minutes = (difference.inMinutes);
    grammar = minutes == 1 ? "minute" : "minutes";
    return "about $minutes $grammar time";
  }
  int seconds = (difference.inSeconds);
  grammar = seconds == 1 ? "second" : "seconds";
  return "about $seconds $grammar time";
}

String SanitiseName(String Input)
{
  return Input.trim().toLowerCase().replaceAll(' ', '');
}

Future<String> UploadPasteee(String Input) {
  Map<String, String> Arguments = new Map<String, String>();
  Arguments["key"] = PasteeApiKey;
  Arguments["description"] = "";
  Arguments["paste"] = Input;
  Arguments["format"] = "json";

  return http.post("https://paste.ee/api", body: Arguments)
  .then((http.Response e) {
    Map response = JSON.decode(e.body);
    print(response);
    return response["paste"]["raw"];
  });
}

Future delay(Duration d, dynamic result) {
  var rnd = new Random();
  Completer completer = new Completer();
  Timer timer = new Timer(d, () => completer.complete(result) );
  return completer.future;
}